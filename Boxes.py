#  File: Boxes.py

#  Description: Finds the largest set of nested boxes within a list of box dimensions

#  Student Name: Christopher Calizzi

#  Student UT EID:csc3322\

#  Course Name: CS 313E

#  Unique Number:50295

#  Date Created:3-8-20

#  Date Last Modified:3-8-20


# generates all subsets of boxes and stores them in all_box_subsets
# box_list is a list of boxes that have already been sorted
# sub_set is a list that is the current subset of boxes
# idx is an index in the list box_list
# all_box_subsets is a 3-D list that has all the subset of boxes
def sub_sets_boxes (box_list, sub_set, idx, all_box_subsets):
    if idx>=len(box_list):
        all_box_subsets.append(sub_set)
    else:
        sub_sets_boxes(box_list, sub_set, idx + 1, all_box_subsets)
        if sub_set:
            set = sub_set + [box_list[idx]]
        else:
            set = [box_list[idx]]
        sub_sets_boxes(box_list,set,idx +1, all_box_subsets)
        return all_box_subsets



# goes through all the subset of boxes and only stores the
# largest subsets that nest in the 3-D list all_nesting_boxes
# largest_size keeps track what the largest subset is
def largest_nesting_subsets (all_box_subsets, largest_size, all_nesting_boxes):
  largest_size  = 0
  for s in all_box_subsets:
      i = 0
      while i<len(s)-1 and does_fit(s[i],s[i+1]):
          i+=1
      if i == len(s) - 1:
          if len(s)>largest_size:
              all_nesting_boxes = [s]
              largest_size = len(s)
          elif len(s)==largest_size:
              all_nesting_boxes.append(s)
  return all_nesting_boxes

# returns True if box1 fits inside box2
def does_fit (box1, box2):
  return (box1[0] < box2[0] and box1[1] < box2[1] and box1[2] < box2[2])

def main():
  # open the file for reading
  in_file = open ("./boxes.txt", "r")

  # read the number of boxes
  line = in_file.readline()
  line = line.strip()
  num_boxes = int (line)

  # create an empty list for the boxes
  box_list = []

  # read the boxes from the file
  for i in range (num_boxes):
    line = in_file.readline()
    line = line.strip()
    box = line.split()
    for j in range (len(box)):
      box[j] = int (box[j])
    box.sort()
    box_list.append (box)

  # close the file
  in_file.close()
  #print (box_list)
  # print()

  # sort the box list
  box_list.sort()
  # print (box_list)
  # print()

  # create an empty list to hold all subset of boxes
  all_box_subsets = []

  # create a list to hold a single subset of boxes
  sub_set = []

  # generate all subsets of boxes and store them in all_box_subsets
  all_box_subsets = sub_sets_boxes (box_list, sub_set, 0, all_box_subsets)

  # initialize the size of the largest sub-set of nesting boxes
  largest_size = 0

  # create a list to hold the largest subsets of nesting boxes
  all_nesting_boxes = []

  # go through all the subset of boxes and only store the
  # largest subsets that nest in all_nesting_boxes
  subsets = largest_nesting_subsets(all_box_subsets, largest_size, all_nesting_boxes)
  subsets.sort()
  # print all the largest subset of boxes
  if subsets:
    if len(subsets) == 1:
      print("Largest Subset of Nesting Boxes:")
      for e in subsets[0]:
        print("( ", end = "")
        for i in e:
          print(i, end = " ")
        print(")")
    else:
      print("Largest Subsets of Nesting Boxes:")
      for r in subsets:
        for e in r:
          print("( ", end = "")
          for i in e:
            print(i, end = " ")
          print(")")
        print()
if __name__ == "__main__":
  main()
